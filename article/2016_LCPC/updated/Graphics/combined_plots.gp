set macro

set terminal pdf enhanced font 'Helvetica,14'
set encoding utf8

set key top right opaque height 1 font 'Helvetica, 12' box lt 8 lw 0.5

set auto fix
set offsets graph 0.02, 0.20, 100, 100

set xlabel "Codewords size (n = log2(N))"
set ylabel "Coded Mbit/s"

set logscale x 2
set xtics ("2" 4, "3" 8, "4" 16, "5" 32, "6" 64, "7" 128, "8" 256, "9" 512, "10" 1024, "11" 2048, "12" 4096, "13" 8192, "14" 16384, "15" 32768, "16" 65536)

set grid lc rgb "#c0c0c0"

set palette model HSV functions gray, 1, 0.7
set cbrange [0:8]
unset colorbox

set pointsize 1

# styles: inter-sse4-3g intra-sse4-3g inter-sse4-1g intra-sse4-1g inter-neon-2g intra-neon-2g ref-inter-sse4-1g
set style line 1 lc palette cb 0 lw 1 pt 4
set style line 2 lc palette cb 1 lw 1 pt 5
set style line 3 lc palette cb 3 lw 1 pt 6
set style line 4 lc palette cb 4 lw 1 pt 7
set style line 5 lc palette cb 6 lw 1 pt 8
set style line 6 lc palette cb 7 lw 1 pt 9
set style line 7 lc rgb "#000000" lw 1.4 pt 2

# 32b rate 1/2

set title "Intra/inter frame vectorization (float), rate 1/2"
datafile = "./samples/samples_32b_rate0.5.dat"
set output "./outputs/mbits_32b_rate05.pdf"
plot for [IDX=0:5] datafile using 1:13 i IDX with lp ls (IDX+1) title columnheader(4)

# 32b rate 5/6

set title "Intra/inter frame vectorization (float), rate 5/6"
datafile = "./samples/samples_32b_rate0.83.dat"
set output "./outputs/mbits_32b_rate083.pdf"
plot for [IDX=0:5] datafile using 1:13 i IDX with lp ls (IDX+1) title columnheader(4)

# 8b rate 1/2

set title "Intra/inter frame vectorization (char), rate 1/2"
datafile = "./samples/samples_8b_rate0.5.dat"
set output "./outputs/mbits_8b_rate05.pdf"
plot for [IDX=0:6] datafile using 1:13 i IDX with lp ls (IDX+1) title columnheader(4)

# 8b rate 5/6

set title "Intra/inter frame vectorization (char), rate 5/6"
datafile = "./samples/samples_8b_rate0.83.dat"
set output "./outputs/mbits_8b_rate083.pdf"
plot for [IDX=0:6] datafile using 1:13 i IDX with lp ls (IDX+1) title columnheader(4)

