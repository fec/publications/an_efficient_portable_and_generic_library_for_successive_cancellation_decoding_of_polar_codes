%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% baposter Portrait Poster
% LaTeX Template
% Version 1.0 (15/5/13)
%
% Created by:
% Brian Amberg (baposter@brian-amberg.de)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[a0paper,portrait,fontscale=0.32]{baposter}
\usepackage[scaled]{helvet}
\usepackage{url}
\renewcommand\familydefault{\sfdefault}

\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{booktabs} % Horizontal rules in tables
\usepackage{relsize} % Used for making text smaller in some places
\usepackage{enumitem} % list item spacing

%\graphicspath{{pics/}} % Directory in which figures are stored

\setlist{leftmargin=*,noitemsep}

\definecolor{bordercol}{RGB}{40,40,40} % Border color of content boxes
\definecolor{headercol1}{RGB}{186,215,230} % Background color for the header in the content boxes (left side)
\definecolor{headercol2}{RGB}{80,80,80} % Background color for the header in the content boxes (right side)
\definecolor{headerfontcol}{RGB}{0,0,0} % Text color for the header text in the content boxes
%\definecolor{boxcolor}{RGB}{186,215,230} % Background color for the content in the content boxes
\definecolor{boxcolor}{RGB}{255,255,255} % Background color for the content in the content boxes

\definecolor{RY1}{RGB}{226,0,38}
\definecolor{RY2}{RGB}{254,205,27}
\definecolor{Bl1}{RGB}{38,109,131}
\definecolor{Bl2}{RGB}{136,204,207}
\definecolor{OY1}{RGB}{236,117,40}
\definecolor{OY2}{RGB}{254,207,83}
\definecolor{DB1}{RGB}{28,70,114}
\definecolor{DB2}{RGB}{0,147,196}
\definecolor{Vi1}{RGB}{114,43,74}
\definecolor{Vi2}{RGB}{198,132,116}
\definecolor{PG1}{RGB}{133,146,66}
\definecolor{PG2}{RGB}{199,213,79}
\definecolor{Gr1}{RGB}{91,94,111}
\definecolor{Gr2}{RGB}{162,181,198}
\definecolor{Gn1}{RGB}{44,152,46}
\definecolor{Gn2}{RGB}{157,193,7}

\newcommand{\R}{\textsuperscript{\textregistered}}
\newcommand{\TM}{\textsuperscript{\texttrademark}}

\begin{document}

\background{ % Set the background to an image (background.pdf)
%\begin{tikzpicture}[remember picture,overlay]
%\draw (current page.north west)+(-2em,2em) node[anchor=north west]
%{\includegraphics[height=1.1\textheight]{figures/background}};
%\end{tikzpicture}
}

\begin{poster}{
grid=false,
%borderColor=bordercol, % Border color of content boxes
%headerColorOne=headercol1, % Background color for the header in the content boxes (left side)
%headerColorTwo=headercol2, % Background color for the header in the content boxes (right side)
%headerFontColor=headerfontcol, % Text color for the header text in the content boxes
%boxColorOne=boxcolor, % Background color for the content in the content boxes
bgColorOne=DB2!60,
bgColorTwo=Gn2!60,
borderColor=Vi1!85,
headerColorOne=Bl2!50,
headerColorTwo=Bl1!50,
headerFontColor=DB1,
boxColorOne=Vi2!05,
headershape=roundedright, % Specify the rounded corner in the content box headers
headerfont=\large\sf\bf, % Font modifiers for the text in the content box headers
textborder=none,
background=shadetb,
headerborder=none,
boxshade=plain
}
{}
%
%----------------------------------------------------------------------------------------
%	TITLE AND AUTHOR NAME
%----------------------------------------------------------------------------------------
%
{\sf\bf\huge An Efficient, Portable and Generic Library\\for Successive Cancellation Decoding of Polar Codes }
%{\sf\bf P-EDGE: Polar ECC Decoder Generation Environment} % Poster title
{\vspace{1em} Adrien Cassagne, Bertrand Le Gal, Camille Leroux, \\Olivier Aumage and Denis Barthou % Author names
} % Author email addresses
{\colorbox{white}{\includegraphics[trim=2cm 1cm 2cm 1cm,clip=true,scale=0.15]{pics/logo}}} % University/lab logo

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\headerbox{Exploring Soft ECC Decoding}{name=introduction,column=0,row=0,headerColorOne=RY2!50,headerColorTwo=RY1!50,headerFontColor=RY1}{

  \textbf{Growing interest for software Error Correction Codes
    implementations}
  \begin{itemize}
    \item Leverage powerful, energy efficient procs.
    \item Reduce dev. cost and time to market
    \item Validate and optimize new algorithms
  \end{itemize}

  Recent \textbf{Successive Cancellation} soft decoders for Polar ECC
  codes strongly benefit from modern CPUs capabilities and SIMD units,
  open the way to a wide optimization range.

  \medskip
  \textbf{Introducing P-EDGE}, an environment for exploring Polar
  ECC decoders.
  \begin{itemize}
    \item Specialized skeleton generator
    \item Building blocks library
  \end{itemize}
}

%----------------------------------------------------------------------------------------
%	MATERIALS AND METHODS
%----------------------------------------------------------------------------------------

\headerbox{Decoding of Polar Codes}{name=methods,column=0,below=introduction}{

The \textbf{Successive Cancellation (SC)} decoding algorithm: a depth-first binary tree traversal algorithm based on 3 key functions:
\begin{eqnarray*}
\left\{\begin{array}{l c l c l}
f(\lambda_a,\lambda_b) &=& sign(\lambda_a.\lambda_b).\min(|\lambda_a|,|\lambda_b|)\\
g(\lambda_a,\lambda_b,s)&=&(1-2s)\lambda_a+\lambda_b\\
h(s_{a}, s_{b}) &=& (s_{a} \oplus s_{b}, s_{b}).
\end{array}\right.
\label{eq:f_g}
\end{eqnarray*}

%------------------------------------------------

\begin{center}
\includegraphics[width=0.55\linewidth]{figs/3_nodes2.pdf}
\captionof{figure}{Per-node downward and upward computations}
\end{center}

%------------------------------------------------

}

%----------------------------------------------------------------------------------------
%	RESULTS 2
%----------------------------------------------------------------------------------------

\headerbox{Results}{name=results2,span=2,column=0,below=methods,above=bottom}{ % To reduce this block to 1 column width, remove 'span=2'

Fig.~\ref{fig:perf} shows \textbf{the performance of the P-EDGE generated
decoders} on various SIMD strategies and on an Intel\R Xeon\R E31225 CPU @ 3.1Ghz
(\emph{Sandy Bridge} architecture). \textbf{Higher is better}.

%------------------------------------------------

\begin{center}
  \includegraphics[width=0.40\textwidth]{graphs/E31225_thr_intra_32b}
  \includegraphics[width=0.40\textwidth]{graphs/E31225_thr_inter_8b}
  \captionof{figure}{32-bit floating point intra frame performance comparison:
  the two cross marks show state-of-the art performance results reported in
  \cite{sarkis2014_2} (left);
  8-bit fixed point performance comparison: circles show P-EDGE results and
  triangles show our former ``handwritten'' implementation
  results~\cite{le-gal2015} (right).}
  \label{fig:perf}
\end{center}

%------------------------------------------------

\textbf{The P-EGDE exploration capabilities} are demonstrated on
Fig.~\ref{fig:opti}: various optimizations can have different impacts on the
performance depending on the code rate and the SIMD strategy we use.

%------------------------------------------------

\begin{center}
  \includegraphics[width=0.85\textwidth]{graphs/E31225_thr_opti}
  \captionof{figure}{Throughput depending on the different optimizations
  (frame size $N = 2048$), for intra-frame vectorization on the left and
  intra-frame vectorization on the right, resp. Compression techniques disabled.}
  \label{fig:opti}
\end{center}

%------------------------------------------------
}
%----------------------------------------------------------------------------------------
%	CHAIN
%----------------------------------------------------------------------------------------
\headerbox{The Communication Chain}{name=chain,span=2,column=1,row=0}{ % To reduce this block to 1 column width, remove 'span=2'
\begin{center}
  \includegraphics[width=0.75\textwidth]{figs/communication_chain}
\end{center}
}
%----------------------------------------------------------------------------------------
%	RESULTS 1
%----------------------------------------------------------------------------------------


\headerbox{P-EDGE Generation Process}{name=results1,span=2,column=1,below=chain,above=results2}{ % To reduce this block to 1 column width, remove 'span=2'

\textbf{The set of the rewriting rules used by P-EDGE} is shown
Fig.~\ref{fig:patterns}: it enables to generate the source code of the decoders.
Electronics scientist can enhance this set.

%------------------------------------------------

\begin{center}
\includegraphics[width=0.575\linewidth]{figs/patterns}
\includegraphics[width=0.375\linewidth]{figs/patterns_example}
\captionof{figure}{Set of the rewriting rules (left); Example of the rewriting rules application (right)}
\label{fig:patterns}
\end{center}

%------------------------------------------------

\bigskip
\textbf{Binary compression is the key to generate high performance decoders.}
Fig.~\ref{fig:compression} presents an example of the P-EDGE sub-tree folding
technique used to reduce the binary size of the generated code (frame size
$N = 128$).

%------------------------------------------------

\begin{center}
\includegraphics[scale=0.12]{figs/Decoder_polar_SC_sys_N128_K64_SNR25}
\includegraphics[scale=0.12]{figs/Decoder_polar_SC_sys_N128_K64_SNR25_short}
\captionof{figure}{Uncompressed decoding tree (left); Compressed decoding tree (right)}
\label{fig:compression}
\end{center}

}

%----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

\headerbox{References}{name=references,below=methods,column=2}{

\smaller % Reduce the font size in this block
\renewcommand{\section}[2]{\vskip 0.05em} % Get rid of the default "References" section title
%\nocite{*} % Insert publications even if they are not cited in the poster

\bibliographystyle{unsrt}
\bibliography{biblio} % Use sample.bib as the bibliography file
}

%----------------------------------------------------------------------------------------
%	CONCLUSION
%----------------------------------------------------------------------------------------

\headerbox{Genericity and Performance}{name=conclusion,column=2,below=references,headerColorOne=RY2!50,headerColorTwo=RY1!50,headerFontColor=RY1}{
  \textbf{Clear separation of concerns}
  \begin{itemize}
    \item Abstract algorithmic level: ECC experts
    \item Architecture dependent level: HPC experts
  \end{itemize}

  \medskip
  \textbf{Qualitative and quantitative benefits}
  \begin{itemize}
    \item Software design, flexibility
    \item Performance on par or exceeding state of art
  \end{itemize}

  \medskip
  \textbf{P-Edge reconciles good programming practices and performance!}

  \medskip
}

%----------------------------------------------------------------------------------------
%	Contact
%----------------------------------------------------------------------------------------

\headerbox{Contact}{name=contact,column=2,below=conclusion,headerColorOne=Gr2!50,headerColorTwo=Gr1!50,headerFontColor=Gr1}{

	\medskip
        Contact e-mail: {\color{DB2}\url{adrien.cassagne@inria.fr}}
	%\smallskip
\begin{center}
  \includegraphics[width=0.33\textwidth]{qr}
\end{center}
	%\smallskip
}


%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------

\headerbox{Acknowledgements}{name=acknowledgements,column=2,below=contact, above=bottom}{

\smaller % Reduce the font size in this block
This study has been carried out with financial support from the French
State, managed by the French National Research Agency (ANR) in the frame
of the "Investments for the future" Programme IdEx Bordeaux - CPU
(\textbf{ANR-10-IDEX-03-02}).
}

%----------------------------------------------------------------------------------------

\end{poster}

\end{document}
