\documentclass[unknownkeysallowed]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{enumerate}
\usepackage{textcomp}
\usepackage{charter}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{url}
\usepackage{graphicx} 
\usepackage{hyperref}
\usepackage{lmodern}
\usepackage{caption}
\usepackage{listings}
\usepackage{color}

%\usetheme{Berlin}
\usetheme{Madrid}
\usecolortheme{sidebartab}
 
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\newcommand{\R}{\textsuperscript{\textregistered}}
\newcommand{\TM}{\textsuperscript{\texttrademark}}

\lstset{
  language=C++,
  basicstyle=\tiny\ttfamily,
  numbers=left,
  numberstyle=\tiny\ttfamily,
  stepnumber=1,
  numbersep=5pt,
  backgroundcolor=\color{white},
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  frame=single,
  tabsize=2,
  captionpos=b,
  breaklines=true,
  breakatwhitespace=false,
  escapeinside={(*@}{@*)},
  identifierstyle=\ttfamily,
  keywordstyle=\color[rgb]{0,0,1},
  commentstyle=\color[rgb]{0.133,0.545,0.133},
  stringstyle=\color[rgb]{0.627,0.126,0.941},
  moredelim=[is][\underbar]{**}{**},
}

\captionsetup{
  %figurename={\sc Fig.},
  figurename=,
  %tablename={\sc Tab.}
  tablename=
}

\beamertemplatenavigationsymbolsempty

\setbeamertemplate{footline}
{%
  \begin{beamercolorbox}[wd=1.0\textwidth,ht=2.6ex,dp=1ex,leftskip=.5em,
    rig htskip=.5em]{author in head/foot}%
    \usebeamerfont{author in head/foot}%
    \insertshortauthor\hfill\insertshortinstitute%
  \end{beamercolorbox}%

  \hspace*{-4.0ex}\hspace*{0.3\textwidth}%

  \begin{beamercolorbox}[wd=1.0\textwidth,ht=2.6ex,dp=1ex,left,leftskip=.5em,
    rightskip=.5em]{title in head/foot}%
  \usebeamerfont{title in head/foot}%
    \insertshorttitle\hfill\insertframenumber{} / \inserttotalframenumber%
  \end{beamercolorbox}%
}

\setbeamertemplate{blocks}[rounded][shadow=true]
\setbeamercolor{block body}{fg=black,bg=blue!30}

\title[P-EDGE: Polar ECC Decoder Generation Environment]{
An Efficient, Portable and Generic Library\\ 
for Successive Cancellation Decoding of Polar Codes}

\author[A. Cassagne \and B. Le Gal \and C. Leroux \and O. Aumage
\and D. Barthou]{
\underline{Adrien Cassagne}\inst{1,2}
\and Bertrand Le Gal\inst{1} \and Camille Leroux\inst{1} \\
\and Olivier Aumage\inst{2} \and Denis Barthou\inst{2}}

\institute[IMS, Inria / Labri, Univ. Bordeaux, INP ] 
{
  \inst{1}%
  IMS, Univ. Bordeaux, INP, France
  \and
  \inst{2}%
  Inria / Labri, Univ. Bordeaux, INP, France
}

\date[LCPC 2015] % (optional)
{LCPC, September 2015}

\titlegraphic{
  \includegraphics[width=1.6cm]{pics/bdx}
  \hspace*{0.25cm}~%
  \includegraphics[width=0.9cm]{pics/excellence}
  \hspace*{0.25cm}~%
  \includegraphics[width=1.0cm]{pics/marianne}
  \hspace*{0.25cm}~%
  \includegraphics[height=0.60cm]{pics/ims}
  \hspace*{0.25cm}~%
  \includegraphics[height=0.60cm]{pics/inria}
  \hspace*{0.25cm}~%
  \includegraphics[height=0.85cm]{pics/labri}
  \hspace*{0.25cm}~%
  \includegraphics[height=0.85cm]{pics/ipb}
}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\section{Introduction}

\subsection{Error Correction Codes}

\begin{frame}
  \frametitle{Context: Error Correction Codes (ECC)}
  \begin{itemize}
    \item Algorithm that enable reliable delivery of digital data
    \begin{itemize}
      \item Redundancy for error correction
    \end{itemize}
    \item Usually implemented in hardware
    \item Growing interest for software implementation
    \begin{itemize}
      \item End-user utilization (low power consumption processors)
      \item Less expensive production than dedicated hardware chips
      \item Algorithms validation (typically Monte-Carlo HPC simulations) 
    \end{itemize}
    \item Require performance
    \item Focus on the decoder (most time consuming part)
  \end{itemize}
  \begin{figure}[htp]
    \centering
    \includegraphics[width=0.8\textwidth]{figs/communication_chain}
    \caption{The communication chain}\label{diagram:communication_chain}
  \end{figure}
\end{frame}

\subsection{Polar codes}

\begin{frame}
  \frametitle{Polar Codes as a New Class of ECC}
  \begin{itemize}
    \item Explored for upcoming 5G Mobile Phones (Huawei\footnote{\tiny{\url{http://www.huawei.com/minisite/has2015/img/5g_radio_whitepaper.pdf}}})
	\item Redundancy: adding bits at fixed positions (value is always 0)
  \end{itemize}

	\begin{figure}[htp]
      \centering
      \includegraphics[width=0.8\textwidth]{figs/frozen_bits2}
      \caption{Example of Polar Code (number of info. bits $K = 4$, frame size $N = 8$)}
      \label{fig:frozen_bits}
    \end{figure}

  \begin{itemize}
    \item Rate $R = N / K$: frame size / information bits ratio   
  \end{itemize}

\end{frame}

\section{Decoding algorithm}

\subsection{Algorithm}

\begin{frame}[containsverbatim]
  \frametitle{The Successive Cancellation (SC) Algorithm}
  \begin{itemize}
    \item Depth-first binary tree traversal/search algorithm
    \item 3 key functions:
      \begin{eqnarray*}\footnotesize
        \left\{\begin{array}{l c l c l}
        \lambda_c &=& f(\lambda_a,\lambda_b) &=& sign(\lambda_a.\lambda_b).\min(|\lambda_a|,|\lambda_b|)\\
        \lambda_c &=& g(\lambda_a,\lambda_b,s)&=&(1-2s)\lambda_a+\lambda_b\\
        (s_{c}, s_{d}) &=& h(s_{a}, s_{b}) &=& (s_{a} \oplus s_{b}, s_{b}).
        \end{array}\right.
        \label{eq:f_g}
      \end{eqnarray*}
  \end{itemize}
  \begin{columns}[t]
    \begin{column}[T]{5.0cm}
      \begin{figure}[htp]
        \centering
        \includegraphics[width=0.80\textwidth]{figs/3_nodes}
        \caption{Per-node downward and upward computations}
        \label{fig:3_nodes}
      \end{figure}
    \end{column}
    \begin{column}[T]{5.0cm}
      \begin{figure}[htp]
        \centering
        \includegraphics[width=1.0\textwidth]{figs/data_representation_SC_decoder}
        \caption{Data layout representation}
        \label{fig:data_layout}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

%\begin{frame}[containsverbatim]
%  \frametitle{The Successive Cancellation (SC) Pseudo-code}
%  \begin{columns}[t]
%    \begin{column}[T]{5.0cm}
%      \begin{figure}[htp]
%        \centering
%        \includegraphics[width=0.85\textwidth]{figs/3_nodes}
%        %\caption{Per-node downward and upward computations}
%        \label{fig:3_nodes_bis}
%      \end{figure}
%    \end{column}
%    \begin{column}[T]{5.0cm}
%      \begin{figure}[htp]
%        \centering
%        \includegraphics[width=1.0\textwidth]{figs/data_representation_SC_decoder_modif}
%        %\caption{Data layout representation}
%        \label{fig:data_layout_bis}
%      \end{figure}
%    \end{column}
%  \end{columns}
%  \begin{lstlisting}[title={Pseudo code}]
%void decode(float l[2N-1], int s[N], int fb[N], int m, int d = 0, int &lid = 0)
%{
%  int ndata = 1 << (m-d); 
%  if (d < m) // is not a leaf
%  { 
%    apply_f(l,    ndata); decode(l+ndata, s,         fb, m, d+1, lid); // left call 
%    apply_g(l, s, ndata); decode(l+ndata, s+ndata/2, fb, m, d+1, lid); // right call 
%    apply_h(s,    ndata);
%  } 
%  else // take a decision
%    *s = ((!fb[lid++]) && (*l < 0)) ? 1 : 0;
%}
%  \end{lstlisting}
%\end{frame}

\begin{frame}
  \frametitle{Polar Decoding Tree}
  \begin{figure}[htp]
    \includegraphics[width=0.6\textwidth]{figs/patterns_example2}
    \caption{Position of the frozen and information bits in the tree}
    \label{fig:patterns_example2}
  \end{figure}
  \begin{itemize}
    \item Same specialized tree for each frame
	\item Frames are independent
  \end{itemize}
\end{frame}


\subsection{Optimization Space}

\begin{frame}
  \frametitle{A Wide Optimization Space}
  \begin{itemize}
    \item Simplification of the computations (tree pruning, rewriting rules)
    \item Vectorization of the node functions ($f$, $g$, $h$)
    \item Optimization on the decoder binary size
    \item Implementation of low level kernels: various instruction sets (SSE, AVX, NEON, etc.)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example: Application of the Rewriting Rules}
  \begin{figure}[htp]
    \includegraphics[width=\textwidth]{figs/patterns_example}
    \caption{Rewriting rules applied to a $N=8$ and $K=4$ frame}
    \label{fig:patterns_example}
  \end{figure}
  \begin{itemize}
    \item Rewriting rules are applied recursively
	\item Repeated application of this rules lead to a simplified tree
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Rewriting Rules and Tree Pruning}
  \begin{figure}[htp]
    \centering
    \includegraphics[width=1.0\textwidth]{figs/patterns}
    \caption{Sub-tree rewriting rules and tree pruning for processing specialization}
    \label{diagram:patterns}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Vectorization}
  \begin{columns}[t]
    \begin{column}[T]{6.0cm}
      Intra frame SIMD strategy:
      \begin{figure}[htp]
        \centering
        \includegraphics[scale=0.625]{figs/intra-simd}
        %\caption{Intra frame SIMD strategy}
        \label{fig:intra-simd}
      \end{figure}
    \end{column}
    \begin{column}[T]{6.0cm}
      Inter frame SIMD strategy:
      \begin{figure}[htp]
        \centering
        \includegraphics[scale=0.625]{figs/inter-simd}
        %\caption{Inter frame SIMD strategy}
        \label{fig:inter-simd}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

%\begin{frame}
%  \frametitle{Vectorization}
%  \begin{columns}[t]
%    \begin{column}[T]{6.0cm}
%      Intra frame SIMD strategy:\\
%      ~~~$+$ low latency\\
%      ~~~$-$ leafs can't be vectorized 
%    \end{column}
%    \begin{column}[T]{6.0cm}
%      Inter frame SIMD strategy:\\
%      ~~~$+$ high throughput\\
%      ~~~$-$ high latency
%    \end{column}
%  \end{columns}
%\end{frame}

\section{P-EDGE}

\subsection{Main principles}

\begin{frame}
  \frametitle{P-EDGE: a Dedicated Framework for Polar Codes}
  \framesubtitle{Features}

%  \begin{itemize}
%    \item Address a wide optimization space
%    \item Relies on code generation
%    \begin{itemize}
%      \item Rewriting rules determine how to generate the code
%      \item Recursive calls are flattened
%      \item Loop sizes are static: can be fully unrolled at compile time
%    \end{itemize}
%    \item Relies on C++ template specialization
%    \begin{itemize}
%      \item Lots of computations are made at compile time
%    \end{itemize}
%    \item Manage several data types
%    \begin{itemize}
%      \item 32-bit floating point arithmetics (\texttt{float})
%      \item 8-bit fixed point arithmetics (\texttt{char})
%    \end{itemize}
%    \item Enforce a clear separation of concerns
%    \begin{itemize}
%      \item Rewriting rules description: electronics scientist
%      \item Low level building blocks: computer scientist
%    \end{itemize}
%  \end{itemize}

  \begin{enumerate}
    \item Code generation
    \begin{itemize}
	    \item Flattening recursive calls 
      \item Rewriting rules
      \item Generation of templated C++
    \end{itemize}
    \item C++ specialization
    \begin{itemize}
      \item Loop unrolling
	    \item Data types
	    \item SIMD
    \end{itemize}
  \end{enumerate}

\end{frame}

\subsection{Code generation}

\begin{frame}[containsverbatim]
  \frametitle{P-EDGE: Code Generation Example}

  \begin{figure}[htp]
    \includegraphics[width=0.65\textwidth]{figs/patterns_example}
    \caption{Generated code for $N=8$ and $K=4$}
  \end{figure}

  \begin{lstlisting}[]
void Generated_SC_decoder_N8_K4::decode() 
{
  //   ------------template args---------------            -std args-  
  //   -types- --funcs-- ----offsets---- -size-            --buffs---
  f   <     R , F , FI  , 0 , 4 ,     8 , 4    > :: apply (  l       );
  rep < B , R , H , HI  , 8 ,         0 , 4    > :: apply (  l , s   );
  gr  < B , R , G , GI  , 0 , 4 , 0 , 8 , 4    > :: apply (  l , s   );
  spc < B , R , H , HI  , 8 ,         4 , 4    > :: apply (  l , s   );
  xo  < B ,     X , XI  , 0 , 4 ,     0 , 4    > :: apply (      s   );
}
  \end{lstlisting}

\end{frame}

%\subsection{Compression}

\begin{frame}
  \frametitle{Reducing the L1I Cache Occupancy}
  \begin{itemize}
    \item Flattening may generate large binaries
    %\begin{itemize}
      \item Binary size grows with the frame size
      \item Performance slowdown when the binary exceeds the L1I cache
    \item Moving offsets from template to function arguments
    \begin{itemize}
      \item Help the compiler to factorize many function calls
    \end{itemize}
    %\end{itemize}
  \end{itemize}
%  \begin{figure}[htp]
%    \includegraphics[width=0.5\textwidth]{graphs/E31225_decoder_bin_sizes_small}
%    \caption{Decoder binary sizes depending on the frame size}
%    \label{fig:bin_dec_sizes_comp}
%  \end{figure}
\end{frame}

%\begin{frame}[containsverbatim]
%  \frametitle{Moving Offsets from Template to Function Arguments}
%
%  More template parameters, large specialized code:
%  \begin{lstlisting}[]
%void Generated_SC_decoder_N8_K4::decode() 
%{
%  //   ------------template args---------------            -std args-  
%  //   -types- --funcs-- ====offsets==== -size-            --buffs---
%  f   <     R , F , FI  , 0 , 4 ,     8 , 4    > :: apply (  l       );
%  rep < B , R , H , HI  , 8 ,         0 , 4    > :: apply (  l , s   );
%  gr  < B , R , G , GI  , 0 , 4 , 0 , 8 , 4    > :: apply (  l , s   );
%  spc < B , R , H , HI  , 8 ,         4 , 4    > :: apply (  l , s   );
%  xo  < B ,     X , XI  , 0 , 4 ,     0 , 4    > :: apply (      s   );
%}
%  \end{lstlisting}
%
%  More function parameters, shorter code:
%  \begin{lstlisting}[]
%void Generated_SC_decoder_N8_K4::decode_small() 
%{
%  //   -----template args------            ---------std args---------  
%  //   -types- --funcs-- -size-            --buffs--- ====offsets====
%  f   <     R , F , FI  , 4    > :: apply (  l       , 0 , 4 ,     8 );
%  rep < B , R , H , HI  , 4    > :: apply (  l , s   , 8 ,         0 );
%  gr  < B , R , G , GI  , 4    > :: apply (  l , s   , 0 , 4 , 0 , 8 );
%  spc < B , R , H , HI  , 4    > :: apply (  l , s   , 8 ,         4 );
%  xo  < B ,     X , XI  , 4    > :: apply (      s   , 0 , 4 ,     0 );
%}
%  \end{lstlisting}
%
%\end{frame}

\begin{frame}
  \frametitle{Sub-tree Folding Technique}
  \begin{figure}[htp]
    \includegraphics[scale=0.215]{figs/Decoder_polar_SC_sys_N128_K64_SNR25}
    \caption{Full decoding tree representation ($N=128, K=64$).}
    \label{fig:full_tree}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Sub-tree Folding Technique}
  \framesubtitle{Enabling compression}
  \begin{figure}[htp]
    \includegraphics[scale=0.215]{figs/Decoder_polar_SC_sys_N128_K64_SNR25_short}
    %\caption{Partial decoding tree representation ($N=128, K=64$)}
    \label{fig:partial_tree}
  \end{figure}
  \begin{itemize}
    \item A single occurrence of a given sub-tree traversal is generated, and reused wherever needed
    \item Compression ratio on the example shown: 1.48
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Binary Sizes Comparison}
  \begin{figure}[htp]
    \includegraphics[width=1\textwidth]{graphs/E31225_decoder_bin_sizes}
    %\caption{Full decoding tree representation ($N=128, K=64$).}
    \label{fig:bin_dec_sizes}
  \end{figure}
  \begin{itemize}
    \item De-templetization: \textbf{10-fold} binary reduction
    \item Tree folding: \textbf{5-fold} binary reduction (for $N = 2^{16}$)
  \end{itemize}
\end{frame}

\section{Experimentations}

\subsection{Throughput performance}

\begin{frame}
  \frametitle{Performance results}
  \begin{table}[ht]
  \begin{center}
  {\footnotesize
    \begin{tabular}{c|c|c|c}
              & P-Edge Intel-based & P-Edge ARM-based & McGill impl.\footnote{
\footnotesize{G. Sarkis, P. Giard, C. Thibeault, and W.J. Gross.
Autogenerating software polar decoders.
In Signal and Information Processing (GlobalSIP), 2014 IEEE Global
Conference on, pages 6–10, Dec 2014.}}\\
      \hline
      CPU      & Intel Xeon E31225 & ARM Cortex-A15 & Intel Core i7-2600 \\
               & 3.10Ghz           & MPCore~2.32GHz & 3.40GHz            \\
      Cache    & L1I/L1D 32KB      & L1I/L1D 32KB   & L1I/L1D 32KB       \\
               & L2 256KB          & L2 1024KB      & L2 256KB           \\
               & L3 6MB            & No L3          & L3 8MB             \\
      Compiler & GNU g++~4.8       & GNU g++~4.8    & GNU g++~4.8        \\ 
      \hline
    \end{tabular}
    }
    \end{center}
    \caption{Performance evaluation platforms.}\label{tab:platforms}
  \end{table}
  \begin{itemize}
    \item Compiler flags: \texttt{-std=c++11 -Ofast -funroll-loops}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Intra-SIMD Comparison with State of Art}
  \begin{figure}[htp]
    \includegraphics[width=1\textwidth]{graphs/E31225_A15_thr_intra_32b}
%    \caption{Performance of several 32-bit FP code rates\\
%      Cross marks: McGill state of art}
    \label{plot:float32_intra_avx}
  \end{figure}

  \begin{itemize}
    %\item Performance of several 32-bit floating point code rates
    \item Cross marks: Sarkis et al.
    \item \textbf{P-Edge up to 25\% better}
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Inter-SIMD Comparison with State of Art}
  \begin{figure}[htp]
    \includegraphics[width=1\textwidth]{graphs/E31225_A15_thr_inter_8b}
%    \caption{Performance comparison between several code rates of 8-bit
%      fixed point decoding stages. Circles show P-EDGE results. Triangles show 
%      former ``handwritten'' implementation results~\cite{le-gal2015}.}
%    \label{plot:int8_inter_sse41}
  \end{figure}

  \begin{itemize}
    %\item Performance of several 8-bit fixed point code rates
    \item Triangles marks: former ``handwritten'' implementation\footnote{\footnotesize
B. Le Gal, C. Leroux, and C. Jego.
Multi-gb/s software decoding of polar codes.
IEEE Transactions on Signal Processing, 63(2):349–359, Jan 2015.}
    \item \textbf{P-Edge up to 25\% better}
  \end{itemize}
\end{frame}

\subsection{Exploring respective optimization impacts with P-EDGE}

\begin{frame}
  \frametitle{Exploring Optimization Impacts}
  \begin{figure}[htp]
    \includegraphics[width=\textwidth]{graphs/E31225_thr_opti.pdf}
    \caption{Throughput depending on the different optimizations for 
      $N = 2048$}
    \label{plot:intra_inter_opti}
  \end{figure} 
\end{frame}

%\begin{frame}
%  \frametitle{Exploring Optimization Impacts}
%  \begin{figure}[htp]
%    \centering
%    \includegraphics[width=0.5\textwidth]{graphs/E31225_thr_inter_intra_32b}
%    \caption{A focus on 32-bit floating point P-EDGE decoder instances}
%    \label{plot:float32_inter_intra_avx}
%  \end{figure}
%  \begin{itemize}
%    \item Inter-SIMD memory footprint is higher than intra-SIMD
%    \item P-EDGE enables to find the most efficient version
%  \end{itemize}
%\end{frame}

\section{Conclusion}

\subsection{Conclusion and future works}

\begin{frame}
  \frametitle{Conclusion and Future Works}
  \textbf{Conclusion}:
  \begin{itemize}
    \item P-EDGE: a Polar ECC decoder exploration framework
    \item Clear separation of concerns
    \begin{itemize}
      \item Rewriting rules engine
      \item Low level building blocks ($f, g, h, rep, spc$, ...)
    \end{itemize}
    \item Large optimization exploration
    \item Outperform state of art decoders
    \item Performance Portability
  \end{itemize}

  \textbf{Future works:}
  \begin{itemize}
    \item In-depth performance analysis
    \begin{itemize}
      \item Performance model (\emph{Roof-line}, \emph{Execution-Cache-Memory} (ECM))
    \end{itemize}
    \item Reduce memory footprint
    \item Explore other Polar code decoder variants
  \end{itemize}
\end{frame}

%\begin{frame}[allowframebreaks]
%  \frametitle{References}
%  \bibliographystyle{plain}
%  \bibliography{biblio.bib}
%\end{frame}

\end{document}
